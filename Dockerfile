# Use an official Python runtime as a parent image
FROM python:3.11-slim

# Set the working directory to /app
WORKDIR /app

# Copy only requirements for efficient building
COPY requirements.txt .

# Install any needed packages specified in requirements.txt
RUN python -m pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents to the container at /app
COPY . .

# Set the default command to run your program
CMD ["python", "-OO", "-m", "mishbot"]
