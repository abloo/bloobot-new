.PHONY: install run docker-build docker-run docker-run-daemon

install:
	python3.11 -m pip install -r requirements.txt

run:
	python3.11 -OO -m mishbot

docker-build:
	docker build -t mishbot-image .
	docker volume create mishbot-volume

docker-run:
	docker run -v mishbot-volume:/app/data mishbot-image

docker-run-daemon:
	docker run -d -v mishbot-volume:/app/data mishbot-image
