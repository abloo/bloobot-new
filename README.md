# Mishbot for the Picke

uses hikari

first install required pip modules: `python -m pip install -r requirements.txt`

to run: `python -OO -m mishbot`
CTRL+C to shut the bot down

## Config

Create a .env file at the root and make sure it has the following values:  
`TOKEN` the bot token  
`TENORKEY` for tenor gif fetching  

## Building and running using make
for normal building and running, use `make install` and `make run`

for building and running using a docker container use `make docker-build` and `make docker-run`  
to instead run it as a daemon use `docker-run-daemon`  
`docker logs bloobot-image` for seeing logs for the bot

## TODO
[ ] Cached db manager needs add guild method to load cache on new guild join


## Features
 [O] Chimp cooldown  
 [X] Tag Replies  
 [O] Chimpboard  
   [X] Chimpboard video implementation  
   [ ] Chimpboard max content length check + cutoff  
 [ ] Mishvot's rot (karma/mana system + server degradation)  
 [ ] Mishbot webhook  
 [ ] Hoolecorp theme  
 [ ] Autospotify playlists  
 [ ] Backward scraping  
 [X] Consider connection pools for aiosqlite  
 [ ] Docstrings, PEP8  
 [X] better datastructure for mastersettings  
 [ ] consider dependency injections?  

 MODULES FOR IMPROVEMENT  
 apscheduler - later executions  
 
 NEED TO STORE:  
 [X] Server settings (chimp, spotify channels + corresponding guild id)  
 [X] Tags to reply (guild id, simple dict)  
 [o] Karma scores (guild_id, member id - karma)  
 [ ] Spotify playlists (playlist to be added to - guild id)  
