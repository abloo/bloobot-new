import os
import aiosqlite
import hikari
import lightbulb

bot = lightbulb.BotApp(
    token=os.environ["TOKEN"],
    intents=hikari.Intents.ALL,
    help_slash_command=True
)

class DataManager:
    '''
    Abstract Class for asyncronous sql database handling using aiosqlite

    Attributes:
    db : str
        path to database
    key_schema : (str, str)
        schema for keys in database in tuple form
    value_schema : (str, str)
        schema for values in database in tuple form
    '''

    def __init__(self, db: str, key_schema: tuple[str, str], value_schema: tuple[str, str]):
        self.db = db
        self.key_schema=key_schema
        self.value_schema=value_schema

    async def _execute_query(self, q: str, p: tuple[str, ...] | None = None):
        '''
        Local method to asyncronously send the SQL query to the database and return the results

        Parameters:
        q : the query as a string
        p : the tuple of parameters to insert into the query wildcards (optional)

        returns:
            set or list of sets - the resulting rows of the query if any
        '''
        async with aiosqlite.connect(self.db) as con:
            cursor = await con.execute(q, p if p else ())

            if q.strip().upper().startswith("SELECT"):
                rows = await cursor.fetchall()
                return rows
            else:
                await con.commit()

    async def add_guild(self, g) -> None:
        '''
        Adds a guild by creating a table for it

        Parameters:
        g : the ID of the guild
        '''
        await self._execute_query(f'CREATE TABLE IF NOT EXISTS "{g}"({self.key_schema[0]} {self.value_schema[1]}, {self.value_schema[0]} {self.value_schema[1]})')#create _cache if no _cache for server exists

    async def update_data(self, g: int, key: str, value: str) -> None:
        '''
        Updates a row in the database

        Parameters:
        g : the ID of the guild
        key : the key (identifying) value in the database
        value : the value value in the database
        '''
        #check if db has setting
        if await self.get_data(g, key):
            await self._execute_query(f'UPDATE "{str(g)}" SET {self.value_schema[0]} = ? WHERE {self.key_schema[0]} = ?', (value, key))
        else:
            await self._execute_query(f'INSERT INTO "{str(g)}" VALUES (?, ?)', (key, value))

    async def get_data(self, g: int, key):
        '''
        Fetches a row in the database

        Parameters:
        g : the ID of the guild
        key : the key (identifying) value in the database
        
        returns:
            set or list of sets - the value for the key in the database
        '''
        rows = await self._execute_query(f'SELECT {self.value_schema[0]} FROM "{str(g)}" WHERE {self.key_schema[0]} = ?', (str(key),))
        return rows

class CachedManager(DataManager):
    '''
    Data manager subclass to include a hashmap to act as a cache. For faster reading to avoid interacting with a file
    Writing still interacts with the file

    The cache structure is as follows:
    {
        guild_id: {
            key: value,
            ...
        },
        ...
    }

    Attributes:
    cache : dict
        hashmap for cache
    db : str
        path to database
    key_schema : (str, str)
        schema for keys in database in tuple form
    value_schema : (str, str)
        schema for values in database in tuple form
    '''
###### TODO
    # ADD AN ADD GUILD METHOD, THE CACHE NEEDS TO BE LOADED ON GUILD JOINED

    def __init__(self, db: str, key_schema: tuple[str, str], value_schema: tuple[str, str]):
        super().__init__(db, key_schema, value_schema)
        self._cache = {}

    async def load_cache(self) -> None:
        '''
        Reads the database file and stores it in the cache hashmap
        If necessary values do not exist, the cache is populated with defaults
        '''
        current_guilds = await bot.rest.fetch_my_guilds()
        for g in current_guilds:
            await self.add_guild(g.id)
            s = await self._execute_query(f'SELECT setting, value FROM "{str(g.id)}"')
            if not s: #if no initialised settings in db (db returns nothing)
                await self.update_data(g.id, "chimpboard_enabled", "False")
                await self.update_data(g.id, "tags_enabled", "False")
            self._cache.update({str(g.id): {x[0]: x[1] for x in s}})

    async def get_data(self, g: int, key):
        '''
        Reads and returns a value for the given key
        Similar to the corresponding superclass method but uses the cache instead
        '''
        return self._cache[str(g)].get(key)

    async def update_data(self, g: int, key: str, value: str) -> None:
        '''
        Updates a row in the database, and in the cache

        Parameters:
        g : the ID of the guild
        key : the key (identifying) value in the database
        value : the value value in the database
        '''
        await super().update_data(g, key, value)
        self._cache[str(g)].update({key: value})

    def print_cache(self, g: str) -> str:
        '''
        Returns a string form of the cache hashmap for the guild
        Usually used to be printed in a message

        Parameters:
        g : the ID of the guild
        '''
        return str(self._cache[str(g)])[1:-1].replace(", ", "\n").replace("'", "")

class SimpleManager(DataManager):
    '''
    Data manager subclass to implement the simplest form of data manager

    Attributes:
    db : str
        path to database
    key_schema : (str, str)
        schema for keys in database in tuple form
    value_schema : (str, str)
        schema for values in database in tuple form
    '''

    async def remove_data(self, g: int, key) -> None:
        await self._execute_query(f'DELETE FROM "{str(g)}" WHERE message = {key}')

# Bot Startup Handler
@bot.listen(hikari.StartedEvent)
async def on_started(event: hikari.StartedEvent) -> None:
    '''
    Bot startup event handler

    Loads bot extensions
    Loads mastersettings db into a global dictionary
    '''
    global mastersettings
    mastersettings = CachedManager("./data/settings.db", ("setting", "TEXT"), ("value", "TEXT"))
    await mastersettings.load_cache()
    bot.load_extensions_from("./mishbot/extensions")


# Bot Join new server handler
@bot.listen(hikari.GuildJoinEvent)
async def on_join_guild(event: hikari.GuildJoinEvent) -> None:
    '''
    Bot new server join event handler

    Adds the new server to the settings dictionary
    Writes the new server initial settings to database

    All modules are disabled by default for the server
    '''
    await mastersettings.add_guild(str(event.guild_id))
    await mastersettings.update_data(event.guild_id, "chimpboard_enabled", "False")
    #tags 


# Bot gets pinged handler
@bot.listen()
async def ping(event: hikari.GuildMessageCreateEvent) -> None:
    '''
    Bot gets pinged event handler

    Simply replies "Pong!"

    Used to check if bot is alive
    '''
    # Do not respond to bots nor webhooks pinging us, only user accounts
    if not event.is_human:
        return

    if bot.get_me().id in event.message.user_mentions_ids:
        await event.message.respond("Pong!")


#Run the bot
def run() -> None:
    '''
    Bot run function
    Also sets the bot status
    '''
    if os.name != "nt":
        import uvloop
        uvloop.install()
    
    bot.run(
        activity=hikari.Activity(
            name="the world burn",
            type=hikari.ActivityType.WATCHING,
        )
    )
