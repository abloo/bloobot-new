import hikari
import lightbulb
import requests
import json
import os
from mishbot.bot import mastersettings, SimpleManager

plugin = lightbulb.Plugin('Chimpboard')

chimpboard = SimpleManager("./data/chimpboard.db", ("message", "INT"), ("post", "INT"))

CHIMP_EMOJI = "🐵"
GIGA_EMOJI = "🦍"

#for handling tenor links
def get_gif_url(tenor_link: str) -> str:
    '''
    Function to get an embeddable tenor gif url with the given tenor link.
    Default tenor links do not embed, you must use the tenor api with a key to fetch the embeddable version.

    Parameters:
    tenor_link : the default tenor gif link as a string

    returns:
        string with the embeddable tenor gif
    '''
    id = tenor_link.split('-')[-1] #get id from link (last set of digits after hypen)
    r = requests.get(f'https://tenor.googleapis.com/v2/posts?ids={id}&key={os.environ["TENORKEY"]}') #send api request for gif

    if r.status_code == 200:
        data = json.loads(r.content)
        return data['results'][0]['media_formats']['mediumgif']['url'] #return standard gif link (plenty to choose from)
    else:
        return tenor_link

async def react_helper(event):
    '''
    General helper function for duplicate code in react add and react remove
    Takes the react event data and returns the formatted strings for the chimpboard message.
    Also fetches relevant data such as the channel for the chimpboard

    Parameters:
    event : the guild reaction event

    Returns:
    c : The channel in the guild for the chimpboard
    post : The post message id if found or already existing in the chimpboard
    chimptally : The formatted string for the current number of chimps
    embed : The formatted embed message to be sent in the chimpboard
    '''
    if event.emoji_name not in [CHIMP_EMOJI, GIGA_EMOJI]:
        return None, None, None, None,

    #check if guild has chimpboard settings enabled
    if (await mastersettings.get_data(event.guild_id,'chimpboard_enabled')=="False" or await mastersettings.get_data(event.guild_id, "chimpboard_channel") is None):
        return None, None, None, None,

    giga_r = await plugin.bot.rest.fetch_reactions_for_emoji(event.channel_id, event.message_id, GIGA_EMOJI) #list of gigachimp reactions
    chimp_r = await plugin.bot.rest.fetch_reactions_for_emoji(event.channel_id, event.message_id, CHIMP_EMOJI) #list of chimp reactions
    c = await mastersettings.get_data(event.guild_id, "chimpboard_channel")
    post = await chimpboard.get_data(event.guild_id, event.message_id) #search for reacted message in chimpboard in db

    if giga_r or len(chimp_r) >= 3:
        m = await plugin.bot.rest.fetch_message(event.channel_id, event.message_id)
        # member version of author
        a = await plugin.bot.rest.fetch_member(event.guild_id, m.author)
        embed = ( #create embed
            hikari.Embed(description=m.content, colour=a.get_top_role().colour)
            .set_author(name=a.display_name, icon=m.author.avatar_url)
            .set_footer(text=m.timestamp.strftime("%d/%m/%Y %H:%M"))
            .add_field("Source", f'[Jump!]({m.make_link(event.guild_id)})')
        )
        # handle images in the messages
        if m.embeds or m.attachments:
            if m.attachments:
                if "image" in m.attachments[0].media_type:
                    embed.set_image(m.attachments[0].url)
                else:
                    embed.description = m.attachments[0].url
            else:
                media = m.content
                if media.startswith("https://tenor.com/view/"): #get tenor gif media from tenor link
                    media = get_gif_url(media)
                embed.set_image(media)

        #create short chimp tally for post
        chimptally = []
        if giga_r:
            chimptally.append(f"{GIGA_EMOJI} **{len(giga_r)}**")
        if chimp_r:
            chimptally.append(f"{CHIMP_EMOJI} **{len(chimp_r)}**")

        return c, post, '  '.join(chimptally), embed
    else:
        return c, post, None, None


@plugin.listener(hikari.GuildReactionAddEvent)
async def on_react(event: hikari.GuildReactionAddEvent) -> None:
    '''
    React event handler on react add

    Uses react_helper() to create/update a chimpboard entry message in the chimpboard channel.
    Also updates data in the chimpboard db
    '''
    c, post, chimptally, embed = await react_helper(event)

    if not chimptally:
        return

    if post: #if no post found in db, create a new one
        message = await plugin.bot.rest.create_message(
            int(c),
            content=chimptally + f" in https://discord.com/channels/{event.guild_id}/{event.channel_id}",
            embed=embed
        )
        # add post to db
        await chimpboard.update_data(event.guild_id, str(event.message_id), str(message.id))
    else: #if post in db, update existing post
        await plugin.bot.rest.edit_message(
            int(c),
            int(post[0][0]),
            content=chimptally + f" in https://discord.com/channels/{event.guild_id}/{event.channel_id}",
            embed=embed
        )

@plugin.listener(hikari.GuildReactionDeleteEvent)
async def on_react_remove(event: hikari.GuildReactionDeleteEvent) -> None:
    '''
    React event handler on react remove

    Uses react_helper() to remove/update a chimpboard entry message in the chimpboard channel.
    Also updates data in the chimpboard db
    '''
    c, post, chimptally, embed = await react_helper(event)

    if not post:
        return

    if chimptally:
    #update message
        await plugin.bot.rest.edit_message(
            int(c),
            int(post[0][0]),
            content=chimptally + f" in https://discord.com/channels/{event.guild_id}/{event.channel_id}",
            embed=embed
        )
    else:
        await plugin.bot.rest.delete_message(int(c), post[0][0]) #delete post
        await chimpboard.remove_data(event.guild_id, event.message_id)

def load(bot):
    bot.add_plugin(plugin)
