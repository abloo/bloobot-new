import hikari
import lightbulb
from mishbot.bot import mastersettings
from mishbot.extensions.chimpboard import chimpboard#, chimpstats
from mishbot.extensions.tags import tags

plugin = lightbulb.Plugin('SlashCommands')

# slash commands for changing server settings or enabling bot modules
#
# settings
#   view
#   chimpboard
#     enable
#     disable
#     channel
#   tags
#       enable
#       disable
#       add
#       remove
#       list

@plugin.command()
@lightbulb.command("settings", "View or modify bot settings")
@lightbulb.implements(lightbulb.PrefixCommandGroup, lightbulb.SlashCommandGroup)
async def settings_group() -> None:
    pass

@settings_group.child
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.command("view", "View the settings for the bot")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_view_settings(ctx: lightbulb.Context) -> None:
    embed = ( #create embed
        hikari.Embed(description=mastersettings.print_cache(str(ctx.guild_id)), colour="#36516c")
    )
    await ctx.respond(embed)

@settings_group.child
@lightbulb.command("chimpboard", "View or modify chimpboard settings")
@lightbulb.implements(lightbulb.PrefixSubGroup, lightbulb.SlashSubGroup)
async def chimpboard_group() -> None:
    pass

@chimpboard_group.child
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.command("enable", "Enable the chimpboard")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_chimpboard_enable(ctx: lightbulb.Context) -> None:
    # enable setting
    await mastersettings.update_data(ctx.guild_id, "chimpboard_enabled", "True")

    # init chimpboard & chimpstats db for guild
    await chimpboard.add_guild(str(ctx.guild_id))
    #chimpstats.add_guild(str(ctx.guild_id))

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description="`Chimpboard` Enabled!\n\n**Please make sure the Chimpboard channel is set using /settings chimpboard channel.**", colour="#36516c")
    )
    await ctx.respond(embed)

@chimpboard_group.child
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.command("disable", "Disable the chimpboard")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_chimpboard_disable(ctx: lightbulb.Context) -> None:
    # disable setting
    await mastersettings.update_data(ctx.guild_id, "chimpboard_enabled", "False")

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description="`Chimpboard` Disabled!", colour="#36516c")
    )
    await ctx.respond(embed)

@chimpboard_group.child
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.option("channel", "The ID of the channel to use as the chimpboard", type=str)
@lightbulb.command("channel", "Set the channel to use as chimpboard")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_chimpboard_channel(ctx: lightbulb.Context) -> None:
    if not (ctx.options.channel.isdigit() and len(ctx.options.channel) == 19):
        await ctx.respond("Error: Value must be a channel ID")
        return

    await mastersettings.update_data(ctx.guild_id, "chimpboard_channel", "False")

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description=f"`Chimpboard` channel set to `{ctx.options.channel}`!", colour="#36516c")
    )
    await ctx.respond(embed)

#   tags
#       enable
#       disable
#       add
#       remove
#       list

@settings_group.child
@lightbulb.command("tags", "View or modify tag settings")
@lightbulb.implements(lightbulb.PrefixSubGroup, lightbulb.SlashSubGroup)
async def tags_group() -> None:
    pass

@tags_group.child
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.command("enable", "Enable server tags")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_tags_enable(ctx: lightbulb.Context) -> None:
    # enable setting
    await mastersettings.update_data(ctx.guild_id, "tagging_enabled", "True")

    # init chimpboard & chimpstats db for guild
    await chimpboard.add_guild(str(ctx.guild_id))
    #chimpstats.add_guild(str(ctx.guild_id))

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description="`Tagging` Enabled!", colour="#36516c")
    )
    await ctx.respond(embed)

@tags_group.child
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.command("disable", "Disable server tags")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_tags_disable(ctx: lightbulb.Context) -> None:
    # disable setting
    await mastersettings.update_data(ctx.guild_id, "tagging_enabled", "False")

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description="`Tagging` Disabled!", colour="#36516c")
    )
    await ctx.respond(embed)

@tags_group.child
@lightbulb.option("message", "The exact message for the bot to reply to", type=str)
@lightbulb.option("reply", "The exact reply for the bot to reply with", type=str)
@lightbulb.command("add", "Set a tag where the bot replies to a certain message with another")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_tags_add(ctx: lightbulb.Context) -> None:
    await tags.update_tag(ctx.guild_id, ctx.options.message, ctx.options.reply, ctx.author.username)

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description=f"Tag `{ctx.options.message}` added!", colour="#36516c")
    )
    await ctx.respond(embed)

@tags_group.child
@lightbulb.option("channel", "The ID of the channel to use as the chimpboard", type=str)
@lightbulb.command("channel", "Set the channel to use as chimpboard")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_tags_remove(ctx: lightbulb.Context) -> None:
    #check if tag exists
    #if::

    if tagauthor := tags.get_author(ctx.guild_id, ctx.options.message) != ctx.author.username:
        embed = ( #create embed
            hikari.Embed(description=f"You are not the author of tag `{ctx.options.message}`! `({tagauthor})` ", colour="#36516c")
        )
        await ctx.respond(embed)
        return

    await tags.remove_tag(ctx.guild_id, ctx.options.message)

    # respond with confirmation message
    embed = ( #create embed
        hikari.Embed(description=f"Tag `{ctx.options.message}` removed!", colour="#36516c")
    )
    await ctx.respond(embed)

@settings_group.child
@lightbulb.command("view", "View the tags for this server")
@lightbulb.implements(lightbulb.PrefixSubCommand, lightbulb.SlashSubCommand)
async def cmd_tags_view(ctx: lightbulb.Context) -> None:
    embed = ( #create embed
        hikari.Embed(description=mastersettings.print_cache(str(ctx.guild_id)), colour="#36516c")
    )
    await ctx.respond(embed)

def load(bot):
    bot.add_plugin(plugin)
