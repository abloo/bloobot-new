import hikari
import lightbulb
from mishbot.bot import mastersettings, DataManager

# schema: msg TEXT, reply TEXT, author TEXT

# cache: {
#   msg: {
#       reply: str
#       author: str
#   },
# }

# eg: {
#   "ping": {
#       reply: "pong!",
#       author: "bloo#3525"
#   },
# }

class TagManager(DataManager):
    def __init__(self):
        self.db = "./data/tags.db"
        self._cache = {}

    async def load_cache(self) -> None:
        current_guilds = await plugin.bot.rest.fetch_my_guilds()
        for g in current_guilds:
            await self.add_guild(g.id)
            s = await self._execute_query(f'SELECT msg, reply, author FROM "{str(g.id)}"')
            if s:
                self._cache.update({str(g.id): {x[0]: {"reply":x[1], "author":x[2]} for x in s}})
            else:
                self._cache.update({str(g.id): {}})


    async def add_guild(self, g) -> None:
        await self._execute_query(f'CREATE TABLE IF NOT EXISTS "{g}"(msg TEXT, reply TEXT, author TEXT)')

    async def update_tag(self, g: int, msg: str, reply: str, author_name: str) -> None:
        #check if db has setting
        if await self.get_data(g, msg):
            await self._execute_query(f'UPDATE "{str(g)}" SET reply = ? WHERE msg = ?', (reply, msg))
            self._cache[str(g)][msg].update({"reply": reply})
        else:
            await self._execute_query(f'INSERT INTO "{str(g)}" VALUES (?, ?, ?)', (msg, reply, author_name))
            self._cache[str(g)].update({msg:{"reply": reply, "author": author_name}})

    async def remove_tag(self, g: int, msg: str) -> None:
        if await self.get_data(g, msg):
            await self._execute_query(f'DELETE FROM "{str(g)}" WHERE msg = {msg}')
            self._cache[str(g)].pop(msg)

    async def get_msg(self, g: int, msg: str):
        return self._cache[str(g)][msg].get("reply")

    async def get_author(self, g: int, msg: str):
        return self._cache[str(g)][msg].get("author")

    def print_cache(self, g: str) -> str:
        return "\n".join(f"{key}: {value['reply']}  |  {value['author']}" for key, value in self._cache[str(g)].items())


plugin = lightbulb.Plugin('Tags')

@plugin.listener(hikari.StartedEvent)
async def on_started(event: hikari.StartedEvent) -> None:
    global tags
    tags = TagManager()
    # on load, read db file into cache dict
    await tags.load_cache()

@plugin.listener(hikari.GuildMessageCreateEvent)
async def on_message(event: hikari.GuildMessageCreateEvent) -> None:
# on message, see if message in dict keys
    # if tags for server enabled
    if r := tags.get_msg(event.guild_id, event.content) is not None:
        # reply with dict reply
        await plugin.bot.rest.create_message(event.channel_id, r[0][0])

def load(bot):
    bot.add_plugin(plugin)
